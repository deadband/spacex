using System;
using System.Collections.Generic;
using System.Linq;

namespace SpaceX
{
    public abstract class Rocket
    {
        public IFuelStrategy landingFuelStrategy;
        public List<RocketPart> RocketParts { set; get; }
        public string Name { get; set; }

        public Rocket(IFuelStrategy landingFuelStrategy)
        {
            this.landingFuelStrategy = landingFuelStrategy;
        }

        internal void BurnFuel()
        {
            RocketParts.Last().BurnFuel();
        }

        internal void Fill()
        {
            RocketParts.ForEach(p => p.Fuel = 100);
        }

        public void Stage_OnFuelBurnt()
        {
            RocketParts.Remove(RocketParts.Last());
            Console.WriteLine($"{Name} stage");
        }

        internal void PrepareToLand()
        {
            while (!(RocketParts.Last() is Lander))
            {
                RocketParts.Remove(RocketParts.Last());
            }

            Console.WriteLine($"{RocketParts.Last().GetType().Name}");
        }
    }
}
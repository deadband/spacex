﻿using System;
using System.Linq;

namespace SpaceX
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //TODO Lądownik, który wyląduje na księżycu a potem na marsie. 
            var Falcon9 = RocketFactory.CreateMarsRocket;

            Falcon9.Fill();
            while (true)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Spacebar:
                            Falcon9.BurnFuel();
                            break;

                    case ConsoleKey.L:
                        Falcon9.PrepareToLand();
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
using System;

namespace SpaceX
{
    public abstract class RocketPart
    {
        public int Fuel { set; get; }
        public decimal Weight { set; get; }

        public event Action OnFuelBurnt;

        protected void BurnFuel(int v)
        {
            if (Fuel <= 0)
            {
                OnFuelBurnt?.Invoke();
            }
            else
            {
                Fuel -= v;
                if (Fuel<0)
                {
                    Fuel = 0;
                }
            }
            Console.WriteLine(Fuel);
        }

        public virtual void BurnFuel()
        {
            this.BurnFuel(10);
        }
    }

    public interface IFuelStrategy
    {
        int FuelFactor { get; }
    }
}
﻿namespace SpaceX
{
    public class Lander : RocketPart
    {
        private readonly IFuelStrategy fuelStrategy;

        public Lander(IFuelStrategy fuelStrategy)
        {
            this.fuelStrategy = fuelStrategy;
        }

        public override void BurnFuel()
        {
            base.BurnFuel(fuelStrategy.FuelFactor);
        }
    }
}
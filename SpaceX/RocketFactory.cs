using System.Collections.Generic;

namespace SpaceX
{
    public static class RocketFactory
    {
        public static Rocket AssemblyRocket(Rocket rocket, string rocketName, int numOfLanders,
            int numberOfStages)
        {
            var rocketParts = new List<RocketPart>();

            rocket.Name = rocketName;
            rocket.RocketParts = rocketParts;

            rocketParts.Add(new CommandModule());

            for (int i = 0; i < numOfLanders; i++)
            {
                rocketParts.Add(new Lander(rocket.landingFuelStrategy));
            }

            Stage stage = new VacuumStage();
            rocketParts.Add(stage);

            for (int i = 1; i < numberOfStages; i++)
            {
                stage = new Stage();
                stage.OnFuelBurnt += rocket.Stage_OnFuelBurnt;
                rocketParts.Add(stage);
            }
            return rocket;
        }

        public static Rocket CreateMoonRocket => AssemblyRocket(new MoonRocket(), "Falcon Heavy", 2, 3);

        public static Rocket CreateMarsRocket => AssemblyRocket(new MarsRocket(), "Falcon Mars Heavy", 3, 5);
    }

    public class MoonRocket : Rocket
    {
        public MoonRocket() : base(new MoonFuelStrategy())
        {
            
        }

    }

    public class MoonFuelStrategy : IFuelStrategy
    {
        public int FuelFactor => 2;
    }

    public class MarsRocket : Rocket
    {
        public MarsRocket() : base(new MarsFuelStrategy())
        {
            
        }
    }

    public class MarsFuelStrategy : IFuelStrategy
    {
        public int FuelFactor => 3;
    }
}
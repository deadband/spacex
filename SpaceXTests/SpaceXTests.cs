﻿using System;
using System.Linq;
using NUnit.Framework;
using SpaceX;
using SpaceXTest;

namespace SpaceXTest
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void RocketMustHaveLander()
        {
            var rocket = RocketFactory.CreateRocket("Falcon9", 1, 2);
            Assert.True(rocket.RocketParts.Any(p => p is Lander));
        }
    }
}